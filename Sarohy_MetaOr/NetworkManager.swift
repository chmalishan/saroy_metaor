//
//  SignUpModel.swift
//  Sarohy_MetaOr
//
//  Created by Ali Shan on 11/26/18.
//  Copyright © 2018 Fiverr. All rights reserved.
//

import Foundation
import UIKit


class NetworkManager{
    
    static var setEquityField : ((String)->())?
    static var setBalanceField : ((String)->())?
    static var setFreeMarginField : ((String)->())?
    static var setFloatingProfitField : ((String)->())?
    
    static var setTodayField : ((String)->())?
    static var setWeekField : ((String)->())?
    static var setMonthField : ((String)->())?
    static var setSinceField : ((String)->())?
    
    static var mt4_account_token: String!
    
    static var equity_temp: String!{
        didSet {
            print("old value \(equity_temp  )")
            setEquityField?(equity_temp)
        }
    }
    
    static var balance_temp: String!{
        didSet {
            print("old value \(balance_temp  )")
            setBalanceField?(balance_temp)
        }
    }
    
    static var free_margin_temp: String!{
        didSet {
            print("old value \(free_margin_temp  )")
            setFreeMarginField?(free_margin_temp)
        }
    }
    
    static var floating_profit_temp: String!{
        didSet {
            print("old value \(floating_profit_temp  )")
            setFloatingProfitField?(floating_profit_temp)
        }
    }
    
    static var today_temp: String!{
        didSet {
            print("old value \(today_temp  )")
            setTodayField?(today_temp)
        }
    }
    
    static var week_temp: String!{
        didSet {
            print("old value \(week_temp  )")
            setWeekField?(week_temp)
        }
    }
    
    static var month_temp: String!{
        didSet {
            print("old value \(month_temp  )")
            setMonthField?(month_temp)
        }
    }
    
    static var since_temp: String!{
        didSet {
            print("old value \(since_temp  )")
            setSinceField?(since_temp)
        }
    }
    
    static func SignUp(email: String, first_name: String, last_name: String){
        
        var request = URLRequest(url: Constants.url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        var err: NSError?
        
        var postString = ""
        let action = "signup"
        let model = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        let deviceInfo = model + "-" + systemVersion
        let device_token = Utils.getDeviceToken()
        let date = Int(Date().timeIntervalSince1970)
        postString.append("action="+action+"&")
        postString.append("email="+email+"&")
        postString.append("first_name="+first_name+"&")
        postString.append("last_name="+last_name+"&")
        postString.append("device_token=" + device_token+"&")
        postString.append("device_info="+deviceInfo+"&")
        postString.append("timestamp="+String(date)+"&")
        
        let signature = Utils.MD5(string: action + email + first_name + last_name + device_token +
            deviceInfo+String(date) + Constants.private_key)

        postString.append("signature="+signature.map { String(format: "%02hhx", $0) }.joined())
        
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("here")
                print("error=\(String(describing: err))")
                return
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }

            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            //Saving device id to player_prefs after succeful invitation
            UserDefaults.standard.set(device_token, forKey: "device_token")
        }
        task.resume()

    }
    
    static func Login(email: String, mt4_account: String, mt4_password: String, mt4_server: String, vc: UIViewController){
        
        var request = URLRequest(url: Constants.url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        var err: NSError?
        
        var postString = ""
        let action = "login"
        let model = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        let deviceInfo = model + "-" + systemVersion
        let device_token = Utils.getDeviceToken()
        let date = Int(Date().timeIntervalSince1970)
        postString.append("action="+action+"&")
        postString.append("email="+email+"&")
        postString.append("mt4_account="+mt4_account+"&")
        postString.append("mt4_password="+mt4_password+"&")
        postString.append("mt4_server="+mt4_server+"&")
        postString.append("device_token="+device_token+"&")
        postString.append("device_info="+deviceInfo+"&")
        postString.append("timestamp="+String(date)+"&")
        
        let signature = Utils.MD5(string: action + email + mt4_account + mt4_password +
            mt4_server + device_token + deviceInfo+String(date) + Constants.private_key)
     
        postString.append("signature="+signature.map { String(format: "%02hhx", $0) }.joined())
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("here")
                print("error=\(String(describing: err))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            
            let dict = convertToDictionary(text: responseString ?? "")
            
            if let dict = dict{
                
                for (key, value) in dict{

                    if (key == "response"){
                        if let result: [String: Any] = value as? [String: Any]{
                            mt4_account_token = result["mt4_account_token"] as? String
                            
                            let check = result["msg"] as? String
                            
                            if (check == "Success"){
                                print("yesssssss")
                                vc.performSegue(withIdentifier: "main_show", sender: vc)
                            }else{
                                print("noooooooo")
                            }
                            
                        }
                    }
                    
                }
            }
            
            GetCurrentInfo(email: email)
        }
            
        task.resume()
        
        
        
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func GetCurrentInfo(email: String){
        
        var request = URLRequest(url: Constants.url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        var err: NSError?
        
        var postString = ""
        
        let action = "get_info"
        let device_token = Utils.getDeviceToken()
        let mt4_account_token = NetworkManager.mt4_account_token!
        let date = Int(Date().timeIntervalSince1970)

        
        postString.append("action="+action+"&")
        postString.append("email="+email+"&")
        postString.append("device_token="+device_token+"&")
        postString.append("mt4_account_token="+mt4_account_token+"&")
        postString.append("timestamp="+String(date)+"&")
        
        let signature = Utils.MD5(string: action + email + device_token + mt4_account_token +
            String(date) + Constants.private_key)

        postString.append("signature="+signature.map { String(format: "%02hhx", $0) }.joined())

        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("here")
                print("error=\(String(describing: err))")
                return
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }

            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            

            let dict = NetworkManager.convertToDictionary(text: responseString ?? "")
            
            if let dict = dict{
             
                NetworkManager.equity_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["account"] as? [String: Any])?["equity"] as? String
                
                NetworkManager.balance_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["account"] as? [String: Any])?["balance"] as? String
                
                NetworkManager.free_margin_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["account"] as? [String: Any])?["free_margin"] as? String
                
                NetworkManager.floating_profit_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["account"] as? [String: Any])?["floating_profit"] as? String
                
                NetworkManager.today_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["profits"] as? [String: Any])?["today"] as? String
                
                NetworkManager.week_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["profits"] as? [String: Any])?["this_week"] as? String
                
                NetworkManager.month_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["profits"] as? [String: Any])?["this_month"] as? String
                
                NetworkManager.since_temp = (((dict["response"] as? [String: Any])?["result"] as? [String: Any])?["profits"] as? [String: Any])?["since_inception"] as? String
                
                
            }
            

        }
        task.resume()

    }
    
}
