//
//  LoginVC.swift
//  Sarohy_MetaOr
//
//  Created by Ali Shan on 11/26/18.
//  Copyright © 2018 Fiverr. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var login_num: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var server_name: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        password.isSecureTextEntry = true
        

        
    }
    
    @IBAction func LoginBtnPressed(_ sender: UIButton) {
        
        print(email.text!)
        print(login_num.text!)
        print(password.text!)
        print(server_name.text!)
        
        NetworkManager.Login(email: email.text!, mt4_account: login_num.text!, mt4_password: password.text!, mt4_server: server_name.text!, vc: self)
    
//        performSegue(withIdentifier: "main_show", sender: self)
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
