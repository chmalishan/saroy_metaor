//
//  SignUpViewController.swift
//  Sarohy_MetaOr
//
//  Created by Ali Shan on 11/26/18.
//  Copyright © 2018 Fiverr. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {

    var signup_model = NetworkManager()
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var first_name: UITextField!
    @IBOutlet weak var last_name: UITextField!
    
    @IBAction func SignUpButtonAction(_ sender: UIButton) {
    
        print(email.text!)
        print(first_name.text!)
        print(last_name.text!)
        
        NetworkManager.SignUp(email: email.text!, first_name: first_name.text!, last_name: last_name.text!)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
