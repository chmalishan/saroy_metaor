//
//  MainScreenVC.swift
//  Sarohy_MetaOr
//
//  Created by Ali Shan on 11/29/18.
//  Copyright © 2018 Fiverr. All rights reserved.
//

import UIKit

class MainScreenVC: UITableViewController {
    

    @IBOutlet var mainScreenTableView: UITableView!
    
    var trade_arr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainScreenTableView.register(TradersCell.self, forCellReuseIdentifier: "traders_title_cell")

        mainScreenTableView.register(Acc_Cell.self, forCellReuseIdentifier: "acc_cell")
        
        mainScreenTableView.register(ProfitCell.self, forCellReuseIdentifier: "profit_cell")
        // Do any additional setup after loading the view.
    }
}

extension MainScreenVC {
 
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return trade_arr.count
        default:
            return 0
        }
    }

//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        switch indexPath.section{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "traders_title_cell", for: indexPath) as? TradersCell {
                cell.setCell(s: "T##String")
                return cell
            }
//        }
        return UITableViewCell()
    }
    
}
